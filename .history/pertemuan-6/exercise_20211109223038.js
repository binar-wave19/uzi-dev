const data = {
    "id": "11644",
    "profile_guid": "kutu_peraturan",
    "subtitle": "PENGESAHAN WIPO COPYRIGHTS TREATY",
    "subtitle_en": "",
    "number": "19",
    "year": 1997,
    "established_at": "1997-05-07T00:00:00.000Z",
    "enforced_at": "1997-05-07T00:00:00.000Z",
    "hierarchy": {
        "key": "keppres",
        "name": "Keputusan Presiden"
    },
    "tags": [
        {
            "guid": "tg-lt5e3d1fb42d1a5",
            "title": "World Intellectual Property Organization",
            "urlPath": [
                {
                    "guid": "lt5e3a46487df95",
                    "slug": "new-category",
                    "title": "New Category"
                },
                {
                    "guid": "lt5e3a46d170f8f",
                    "slug": "bidang-usaha",
                    "title": "Bidang Usaha"
                },
                {
                    "guid": "lt5e3d1fb3aee77",
                    "slug": "perdagangan-internasional",
                    "title": "Perdagangan Internasional"
                },
                {
                    "guid": "lt5e3d1fb3bda84",
                    "slug": "kerjasama-perdagangan-internasional",
                    "title": "Kerjasama Perdagangan Internasional"
                }
            ]
        }
    ],
    "catalog_access_right": "authorized",
    "parent_folders": [
        {
            "id": "lt5e3a46487df95",
            "name": "Pusat Data",
            "name_en": "New Category",
            "weight": 1
        },
        {
            "id": "lt5e3a46bb17f55",
            "name": "Bidang Hukum",
            "name_en": "Legal Sector",
            "weight": 2
        },
        {
            "id": "lt5e3a98396bad5",
            "name": "Hak Atas Kekayaan Intelektual",
            "name_en": "Intellectual Property Rights",
            "weight": 3
        },
        {
            "id": "lt5e3a9974edec1",
            "name": "Hak Cipta",
            "name_en": "Copyright",
            "weight": 4
        }
    ]
}

console.log(data.tags.urlPath[0].name)
