class Animal {
    constructor(nama, makanan, harga){
        this.nama = nama;
        this.makanan = makanan;
        this.harga = harga;
    }
    makan(){
        console.log(`${this.nama} makan ${this.makanan}`);
    }
    
}

class Bird extends Animal { 
    terbang(){
        console.log(`${}`)
    }
}