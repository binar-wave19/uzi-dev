
//  PARENT CLASS
class Karyawan { 
    constructor(nama, gaji){
        this.nama = nama;
        this.gaji = gaji;
    }
    kirimGaji(){
        console.log(`${this.nama} dapat kirim gaji` )
    }

}
class Manager extends Karyawan {
    tunjanganRumah(){
        console.log(`${this.nama} dapat tunjangan rumah` )
    }
}

const junaidi = new Manager("junaidi", 20);
junaidi.tunjanganRumah();
junaidi.kirimGaji();

// zaky.kirimGaji();
// console.log(zaky.kirimGaji());

// inheritas 


