const karyawan1 = {
    nama: "zaky",
    gaji: 10
}
const karyawan2 = {
    nama: "za",
    gaji: 10
}


class Karyawan { 
    constructor(nama, gaji){
        this.nama = nama;
        this.gaji = gaji;
    }
    kirimGaji(){
        return this.gaji;
    }

}
class Manager extends Karyawan {
    constructor(nama, gaji){
        this.nama = nama;
        this.gaji = gaji;
    }
    tunjanganRumah(){
        console.log(`${this.nama} dapat tunjangan rumah` )
    }
}

const zaky = new Karyawan("zaky", 20, "12", "Aceh", "HRD");
const junaidi = new Manager("junaidi", 20);
junaidi.tunjanganRumah();
junaidi.kirimGaji();

// zaky.kirimGaji();
// console.log(zaky.kirimGaji());

// inheritas 


