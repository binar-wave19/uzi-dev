const karyawan1 = {
    nama: "zaky",
    gaji: 10
}
const karyawan2 = {
    nama: "za",
    gaji: 10
}


class Karyawan { 
    constructor(nama, gaji, id, alamat, departemen){
        this.nama = nama;
        this.gaji = gaji + 5;
        this.id = id;
        this.departemen = departemen || "IT";
        this.alamat = alamat || "indonesia";
    }
    kirimGaji(){
        return this.gaji;
    }
    tunjanganRumah(){
        console.log(`${this.nama} dapat tunjangan rumah` )
    }
}

const zaky = new Karyawan("zaky", 20, "12", "Aceh", "HRD");
// zaky.kirimGaji();
// console.log(zaky.kirimGaji());

// inheritas 
const junaidi = new Karyawan("junaidi", 30);

