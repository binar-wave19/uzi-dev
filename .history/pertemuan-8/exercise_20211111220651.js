// 1. buat class dengan nama Hewan, yang memiliki properties (namaHewan, jenisMakanan, harga);
// 2. di class Hewan buat sebuah method makan(), yang ketika dijalankan akan keluar `jerapah makan rumput`;
// 3. buat class turunan dari class Hewan dengan nama Burung;
// 4. didalam class burung terdapat method terbang(), yang ketika di jalanin keluar `pipit terbang`;
class Animal {
    constructor(nama, makanan, harga){
        this.nama = nama;
        this.makanan = makanan;
        this.harga = harga;
    }
    makan(){
        console.log(`${this.nama} makan ${this.makanan}`);
    }
    
}

class Bird extends Animal { 
    constructor(nama, makanan, harga){
        super(nama, makanan);
        this.nama = nama;
        this.makanan = makanan;
        this.harga = harga;
    }
    
    terbang(){
        console.log(`${this.nama} terbang`)
    }
}
const jerapah = new Animal("Jerapah", "rumput", 1000000000 );
console.log()