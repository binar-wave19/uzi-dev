
//  PARENT CLASS
class Karyawan { 
    constructor(nama, gaji){
        this.nama = nama;
        this.gaji = gaji;
    }
    kirimGaji(){
        console.log(`${this.nama} dapat kirim gaji` )
    }

}
// CHILD CLASS
class Manager extends Karyawan {
    constructor(nama, gaji, sekretaris){
        super(nama, gaji);
        this.sekretaris = sekretaris;
        console.log(``)
    }
    tunjanganRumah(){
        console.log(`${this.nama} dapat tunjangan rumah` )
    }
}

const junaidi = new Manager("junaidi", 20, "Ani");
junaidi.tunjanganRumah();
junaidi.kirimGaji();

// zaky.kirimGaji();
// console.log(zaky.kirimGaji());

// inheritas 


