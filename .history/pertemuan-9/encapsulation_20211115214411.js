// Encapsulation Private, protected, public 
// PRIVATE with #
class Employee { 
    #employeeSalary
    static employeeBonus = 0;
    constructor(name, salary, departemen){
        this.employeeName = name;
        this.#employeeSalary = salary;
        this.employeeDepartemen = departemen;
    }
    #calculateSalary(){
        return this.#employeeSalary + 5;
    }
    sendSalary(){
        return `kirim gaji ke ${this.employeeName} sebesar ${this.#calculateSalary()}`
    }
    _insurance(){ //contorh protected
        return "IP 500)"
    }

}
class Manager extends Employee {
    constructor(name, salary, department){
        super(name, salary, department);
    }
    // annualBonus(){
    //     const salaryAfterCalculate = super.#calculateSalary(); 
    //     return salaryAfterCalculate * 2;
    // }
    getInsurance() {
        const insurance = super._insurance();
        return insurance;
    }
}
class SekretarisManager extends Manager{
    cos
}
const zaky = new Employee("zaky", 10, "IT");
console.log(zaky.employeeSalary);

console.log(zaky.sendSalary())

const suselo = new Manager("Suselo", 20, "IT");
console.log(suselo.getInsurance())