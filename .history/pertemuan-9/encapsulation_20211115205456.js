// Encapsulation Private, protected, public 
// PRIVATE with #
class Employee { 
    #employeeSalary
    static employeeBonus = 0;
    constructor(name, salary, departemen){
        this.employeeName = name;
        this.#employeeSalary = salary;
        this.employeeDepartemen = departemen;
    }
    sendSalary(){
        return `kirim gaji ke ${this.employ} sebesar ${this.employeeName}`
    }

}
const zaky = new Employee("zaky", 10, "IT");
console.log(zaky.employeeSalary);

console.log(zaky.sendSalary())