// Encapsulation Private, protected, public 
// PRIVATE with #
class Employee { 
    #employeeSalary
    static employeeBonus = 0;
    constructor(name, salary, departemen){
        this.employeeName = name;
        this.#employeeSalary = salary;
        this.employeeDepartemen = departemen;
    }
    #calculateSalary(){
        return this.#employeeSalary + 5;
    }
    sendSalary(){
        return `kirim gaji ke ${this.employeeName} sebesar ${this.#calculateSalary()}`
    }

}
class Manager extends Employee {
    constructor(name, salary, department){
        super(name, salary, department)
    }
}
const zaky = new Employee("zaky", 10, "IT");
console.log(zaky.employeeSalary);

console.log(zaky.sendSalary())